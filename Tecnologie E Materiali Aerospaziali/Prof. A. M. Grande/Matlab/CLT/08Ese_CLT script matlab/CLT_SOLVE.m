%=============================================================
% CLT_SOLVE : Determina lo stato di deformazione e lo stato
%             di sforzo nelle lamine
%=============================================================
%
%=============================================================
% INPUT: Richiede il file binario in cui CLT_MAKE ha memorizzato
%        le caratteristiche del laminato (.mat) e
%        carica la struttura dati L
%=============================================================
Laminato=input('Nome File Laminato (senza estensione) :','s');
load(Laminato);
%----------------------------------------------------
% Numero di lamine: Scalare -> L.N
% Files Caratteristiche Elastiche e Spessori (Tipologia delle lamine): Array [N] di stringhe -> L.Lamine
% Angoli di Orientamento : Array -> L.Angoli
% Matrici di Rigidezza delle lamine in coordinate lamina: Array [N] di Matrici 3 x 3 -> L.Rigidezze
% Quote z rispetto alla supeficie media geometrica del laminato: Array [N+1] -> L.z
% Spessore del laminato: Scalre -> L.TH
% Sottomatrice A [3,3] -> L.A
% Sottomatrice B [3,3] -> L.B
% Sottomatrice D [3,3] -> L.D
%----------------------------------------------------
%=============================================================
% INPUT: Carichi Generalizzati
%        F=[Nx Ny Nxy Mx My Mxy]'
%=============================================================
F=input('Vettore dei Carichi generalizzati [Nx Ny Nxy Mx My Mxy] = ');
F=F';
%=============================================================
% STRUTTURA DATI OUTPUT
%=============================================================
%------------------------------------------------------------------------
% Deformazioni generalizzate
% U=[eps0x eps0y eps0xy kx ky kxy]'
%------------------------------------------------------------------------
U=[0 0 0 0 0 0]';
%------------------------------------------------------------------------
% Stato di deformazione e sforzo della singola lamina in coordinate lamina
%------------------------------------------------------------------------
EPS=zeros(3,L.N);
SIG=zeros(3,L.N);

%=============================================================
% DEFORMAZIONI GENERALIZZATE
%=============================================================
U=inv([L.A L.B; L.B L.D])*F;
%=============================================================
% STATO SFORZO-DEFORMAZIONE SINGOLE LAMINE IN COORDINATE LAMINA
%=============================================================
for (lamina=1:L.N)
    z=(L.z(lamina)+L.z(lamina+1))/2;
    EPS(:,lamina)=U(1:3) + z*U(4:6);
    EPS(:,lamina)=Rotazione_Deformazioni(EPS(:,lamina),L.Alfa(lamina));
    SIG(:,lamina)=L.Q_lamina(:,:,lamina)*EPS(:,lamina);
end
%=============================================================
% STAMPA RISULTATI
%=============================================================
% LAMINATO
display(U);
pause;
display(SIG);
display(EPS);
pause;
% LAMINE
%for (lamina=1:L.N)
%display(['Lamina : ',num2str(lamina),' Orientamento : ',num2str(L.Alfa(lamina)), ' deg',...
%'  File: ',L.Lamine(lamina,:)]);
%display('Deformazione in Coordinate Lamina : '),
%display(EPS(:,lamina));
%display('Sforzo in Coordinate Lamina : '),
%display(SIG(:,lamina));
%pause;
%end
%=============================================================
% MEMORIZZAZIONE STATO DI SFORZO
%=============================================================
file=input('Nome file Stato di Sforzo-Deformazione (senza estensione) : ','s');
save(file,'L','F','SIG','EPS');
