% Copyright (C) 2013
% Andrea Zanoni <andrea.zanoni@polimi.it>
% 
% Dinamica di Sistemi Aerospaziali
% Esercitazione 5 - analisi numerica

close all
clear all
clc


%% Parametri

global a b d c0 tf L

L = 0.25; 
b = 0.522;
d = 0.1;
a = L;
c0 = 0.5; % nella configurazione iniziale

dt = 0.01;      % intervallo temporale
tf = 3;         % tempo finale simulazione

% Cinematica Diretta - spostamento del carrello imposto, incognite alpha, beta
%% posizioni
t = 0:dt:tf;

% legge oraria del moto del carrello: 
x_min = 0.;
x_max = 0.15;

% 1 - rampa lineare
% xt = x_min + (x_max - x_min).*t/tf;
% xdot = ones(length(xt), 1)*(x_max - x_min)/tf;      % xdot = -cdot

% 2 - mezzo coseno
xt = (x_max + x_min)/2 + (x_max - x_min)/2*(cos((2*pi)/(2*tf)*t - pi)); 
xdot = (x_max - x_min)/2*((-2*pi)/(2*tf)*sin((2*pi)/(2*tf)*t - pi));
xddot = (x_max - x_min)/2*((-4*pi^2)/(4*tf^2)*cos((2*pi)/(2*tf)*t - pi));
c = c0 - xt;

% soluzione tramite fsolve
alpha = zeros(length(xt), 1);
beta = zeros(length(xt), 1);

x0 = [3/2*pi pi];
for ii=1:length(xt)
    yy = fsolve(@(x)cinfun(x, c(ii)), x0);
    alpha(ii) = yy(1);
    beta(ii) = yy(2);
end

figure 
title('cinematica - posizioni')
plot(c, alpha, c, beta)
grid on
xlabel('c [m]', 'FontSize', 12)
ylabel('\alpha,  \beta [rad]', 'FontSize', 12)
hl = legend('\alpha', '\beta');
set(hl, 'FontSize', 12)

%% velocità e accelerazioni

% qui il sistema è lineare, soluzione più semplice. Si fa in una sola
% passata per riutilizzare il calcolo dello Jacobiano

dot_alpha = zeros(length(xt), 1);
dot_beta = zeros(length(xt), 1);

ddot_alpha = zeros(length(xt), 1);
ddot_beta = zeros(length(xt), 1);

for ii = 1:length(xt)
     
    bb = [ xdot(ii);
          0 ];
     
     J = [-a*sin(alpha(ii)) -b*sin(beta(ii));
           a*cos(alpha(ii))  b*cos(beta(ii))];
       
      vel = J\bb;
      
      dot_alpha(ii)  = vel(1);
      dot_beta(ii)   = vel(2);
      
      bbb = [ xddot(ii);
             0 ];
      
      J2 = [ -J(2,1)  -J(2,2);
              J(1,1)   J(1,2)];
      
      acc = J\(bbb - J2*vel.^2);
      
      ddot_alpha(ii) = acc(1);
      ddot_beta(ii) = acc(2);
      
end

figure
title('cinematica - velocita''', 'FontSize', 14)
plot(t, dot_alpha, t, dot_beta)
grid on
xlabel('$\dot{c} \; [m]$', 'Interpreter', 'latex', 'FontSize', 12)
ylabel('$\dot{\alpha},  \dot{\beta} \; [\mathrm{rad}]$', 'Interpreter', 'latex', 'FontSize', 12)
hl = legend('$\dot{\alpha}$',  '$\dot{\beta} \; [\mathrm{rad}]$');
set(hl, 'Interpreter', 'latex', 'FontSize', 12)

figure
title('cinematica - accelerazioni', 'FontSize', 14)
plot(t, ddot_alpha, t, ddot_beta)
grid on
xlabel('t [s]', 'FontSize', 12)
ylabel('$\ddot{\alpha},  \ddot{\beta} \; [\mathrm{rad}]$', 'Interpreter', 'latex', 'FontSize', 12)
hl = legend('$\ddot{\alpha}$',  '$\ddot{\beta} \; [\mathrm{rad}]$');
set(hl, 'Interpreter', 'latex', 'FontSize', 12)

%% posizione, velocita'� e accelerazione di A

pos_A = 2*L*exp(1i*(alpha-pi));
vel_A = (dot_alpha).*(2*L*exp(1i*(alpha-pi/2)));
acc_n_A = dot_alpha.^2.*(2*L*exp(1i*(alpha)));
acc_t_A = ddot_alpha.*(2*L*exp(1i*(alpha - pi/2))); 
acc_A = acc_n_A + acc_t_A;

figure
hold on
title('traiettoria e velocita'' di A', 'FontSize', 14)
% plot traiettoria
plot(pos_A, 'b', 'LineWidth', 2)
plot(pos_A(1), 'sk', 'MarkerSize', 5)
plot(pos_A(end), 'dk', 'MarkerSize', 5)
hl = legend('traiettoria', 'inizio', 'fine');
set(hl, 'FontSize', 12);
axis equal

% plot velocità: vettori centrati in pos_A tangenti alla traiettoria
for ii = 1:30:length(pos_A)
    quiver(real(pos_A(ii)), imag(pos_A(ii)), real(vel_A(ii)), imag(vel_A(ii)), 'r')
end
grid on
xlim([-.33 .02])

figure
hold on
title('traiettoria e accelerazione di A')
% plot traiettoria
plot(pos_A, 'b', 'LineWidth', 2)
plot(pos_A(1), 'sk', 'MarkerSize', 5)
plot(pos_A(end), 'dk', 'MarkerSize', 5)
legend('traiettoria', 'inizio', 'fine')
axis equal
% plot accelerazioni: vettori centrati in pos_A tangenti e normali alla
% traiettoria

% accelerazione tangente
for ii = 1:30:length(pos_A)
    quiver(real(pos_A(ii)), imag(pos_A(ii)), real(acc_t_A(ii)), imag(acc_t_A(ii)), 'r')
    quiver(real(pos_A(ii)), imag(pos_A(ii)), real(acc_n_A(ii)), imag(acc_n_A(ii)), 'b')
end
grid on
xlim([-.33 .02])


%% Dinamica - integrazione numerica dell'equazione di moto

global Mc Ja Ma F_max F_min g tF dtF Fc_sweep

% parametri inerziali 
Mc = 1.;        % [kg]
Ja = 0.08333;   % [kg*m^2]
Ma = 1.;        % [kg]
F_min = 0;      
F_max = 100;
tfin = 60;

% accelerazione di gravita'
g = 9.81;       % [m/s^2]

% integrazione equazione di moto con Runge-Kutta 4-5 ordine. x0 rappresenta
% il vettore delle condizioni iniziali

% sweep in frequenza
dtF = 0.01;
tF = 0:dtF:tfin;
Fc_sweep = chirp(tF, 0.1, tF(end), 30, 'linear');

x0 = [dot_alpha(1) alpha(1)]';
[~, x] = ode45(@dinfun, tF, x0);

% plot risultati
figure
title('Dinamica - risultati', 'FontSize', 14)
plot(tF, x)
hl = legend('$\alpha$', '$\dot{\alpha}$');
set(hl, 'FontSize', 12);
set(hl, 'Interpreter', 'latex', 'FontSize', 12);
grid on
xlabel('t [s]', 'FontSize', 12)
ylabel('$\dot{alpha}, \{\alpha}$', 'Interpreter', 'latex', 'FontSize', 12)
