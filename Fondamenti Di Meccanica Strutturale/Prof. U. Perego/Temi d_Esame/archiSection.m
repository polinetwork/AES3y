%% GEOMETRY PARAMETERS b_i, h_i, y_g_i, shape: "r"=rectangle, "t"=triangle

% geom = {42 6 3 "r";
%         6 42 27 "r";
%         18 6 51 "r"}
% N = 3528;
% T_y = -2880; %[N]
% M_x = 1411200 %[N*mm]

close all
clear all
format longG

%%%%%%%%% INSERT DATA HERE %%%%%%%%%
geom = {300 80 0 460;
        100 190 0 325;
        50 190 -125 325;
        50 190 +125 325;
        300 50 0 205;
        50 100 -125 130;
        50 100 +125 130;
        300 80 0 40};
 
N = 0; %[N]
T_y = 410000; %[N]
M_x = cos(pi/6)*100*1e+9; %[N*mm]
M_y = sin(pi/6)*100*1e+9; %[N*mm]
%%%%%%%%% INSERT DATA HERE %%%%%%%%%

 
b = cell2mat(geom(:,1));
h = cell2mat(geom(:,2));
x = cell2mat(geom(:,3));
y = cell2mat(geom(:,4));

n = length(b);
A_i_vect = [];

%% CALCULATE CG AND TOTAL AREA
for i = 1:n
   
    A_i = b(i)*h(i);
    
    
    A_i_vect = [A_i_vect; A_i];    
end
S_x = sum(A_i_vect.*x);
S_y = sum(A_i_vect.*y);
A = sum(A_i_vect);
x_g = S_x/A;
y_g = S_y/A;


%% CALCULATE MOMENT OF INERTIA
J_x_vect = [];
J_y_vect = [];
for i = 1:n
   
    c = 1/12;
    
    J_x_i = c*b(i)*(h(i))^3 + A_i_vect(i)*(y(i)-y_g)^2;
    J_x_vect = [J_x_vect; J_x_i];
    
    J_y_i = c*(b(i))^3*h(i) + A_i_vect(i)*(x(i)-x_g)^2;
    J_y_vect = [J_y_vect; J_y_i];    
end

J_x = sum(J_x_vect);
J_y = sum(J_y_vect);

sigma = @(x,y) N/A + (M_x/J_x).*y - (M_y/J_y).*x;
sigmaA = sigma(-150, -257.27)
sigmaC = sigma(150, 500-257.27)


%%
b = 300
S_x = @(s) 150*s.^2-23190*s+34000

tau = @(s) (T_y/(J_x*b)) * S_x(s);
tau(0)
tau(25)
tau(50)






%%


% fprintf('\nA = %.1f mm^2\nS_x = %.1f mm^3\ny_g = %.2f mm\nI_u = %.1f mm^4\nM = %.1f N*mm\ny_n = %.2f mm\nsigma_sup = %.2f MPa\nsigma_inf = %.2f MPa', A, S_y, y_g, J_x, M_x, y_n, sigma_sup, sigma_inf)

        
%     yBot = h_tot - y_g;
% if (y_g+y_n) > yBot %note: y_n with sign
%     y_sup = - y_g;
% else
%     y_sup = yBot
% end
%     
% sigma_max = sigma(y_sup)



% t = 6 %[mm] (thickness)
% s_star = -20.95;
% sigma_star = sigma(s_star) %[MPa]
% 
% S_x = @(s) 6*s.^2 - 287.4*s-4227.3;
% S_x_star = S_x(s_star);
% tau_star = abs(T_y*S_x_star/(J_x*t)) %[MPa]
% 
% sigma_vM_star = sqrt(sigma_star^2+3*tau_star^2)


